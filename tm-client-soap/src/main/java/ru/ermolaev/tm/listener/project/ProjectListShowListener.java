package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.ProjectDTO;
import ru.ermolaev.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;

import java.util.List;

@Component
public class ProjectListShowListener extends AbstractProjectListener {

    @Autowired
    public ProjectListShowListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint
    ) {
        super(projectEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    @EventListener(condition = "@projectListShowListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[PROJECTS LIST]");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects();
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(new StringBuilder()
                    .append((projects.indexOf(project) + 1))
                    .append(". {id: ")
                    .append(project.getId())
                    .append("; name: ")
                    .append(project.getName())
                    .append("; description: ")
                    .append(project.getDescription())
                    .append("}"));
        }
        System.out.println("[COMPLETE]");
    }

}
