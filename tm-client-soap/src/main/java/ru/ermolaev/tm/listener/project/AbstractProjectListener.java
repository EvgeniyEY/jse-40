package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    protected ProjectSoapEndpoint projectEndpoint;

    @Autowired
    public AbstractProjectListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint
    ) {
        this.projectEndpoint = projectEndpoint;
    }

}
