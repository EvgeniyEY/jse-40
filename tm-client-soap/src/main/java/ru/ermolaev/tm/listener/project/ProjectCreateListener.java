package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.ProjectDTO;
import ru.ermolaev.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class ProjectCreateListener extends AbstractProjectListener {

    @Autowired
    public ProjectCreateListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint
    ) {
        super(projectEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new project.";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectEndpoint.createProject(projectDTO);
        System.out.println("[COMPLETE]");
    }

}
