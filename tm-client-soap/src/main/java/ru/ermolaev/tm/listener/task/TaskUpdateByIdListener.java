package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.TaskDTO;
import ru.ermolaev.tm.endpoint.soap.TaskSoapEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdListener extends AbstractTaskListener {

    @Autowired
    public TaskUpdateByIdListener(
            @NotNull final TaskSoapEndpoint taskEndpoint
    ) {
        super(taskEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskEndpoint.updateTaskById(taskDTO);
        System.out.println("[COMPLETE]");
    }

}
