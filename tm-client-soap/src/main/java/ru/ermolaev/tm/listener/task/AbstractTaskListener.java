package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.TaskSoapEndpoint;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    protected TaskSoapEndpoint taskEndpoint;

    @Autowired
    public AbstractTaskListener(
            @NotNull final TaskSoapEndpoint taskEndpoint
    ) {
        this.taskEndpoint = taskEndpoint;
    }

}
