package ru.ermolaev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.config.ApplicationConfiguration;

public class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext clientContext =
                new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        @NotNull final Bootstrap bootstrap = clientContext.getBean(Bootstrap.class);
        clientContext.registerShutdownHook();
        bootstrap.run(args);
    }

}
