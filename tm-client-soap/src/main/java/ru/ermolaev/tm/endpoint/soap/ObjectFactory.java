
package ru.ermolaev.tm.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ermolaev.tm.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CountAllProjects_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "countAllProjects");
    private final static QName _CountAllProjectsResponse_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "countAllProjectsResponse");
    private final static QName _CreateProject_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "createProject");
    private final static QName _CreateProjectResponse_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "createProjectResponse");
    private final static QName _FindAllProjects_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "findAllProjects");
    private final static QName _FindAllProjectsResponse_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "findAllProjectsResponse");
    private final static QName _FindProjectById_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "findProjectById");
    private final static QName _FindProjectByIdResponse_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "findProjectByIdResponse");
    private final static QName _RemoveProjectById_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "removeProjectById");
    private final static QName _RemoveProjectByIdResponse_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "removeProjectByIdResponse");
    private final static QName _UpdateProjectById_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "updateProjectById");
    private final static QName _UpdateProjectByIdResponse_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "updateProjectByIdResponse");
    private final static QName _Exception_QNAME = new QName("http://soap.endpoint.tm.ermolaev.ru/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ermolaev.tm.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CountAllProjects }
     * 
     */
    public CountAllProjects createCountAllProjects() {
        return new CountAllProjects();
    }

    /**
     * Create an instance of {@link CountAllProjectsResponse }
     * 
     */
    public CountAllProjectsResponse createCountAllProjectsResponse() {
        return new CountAllProjectsResponse();
    }

    /**
     * Create an instance of {@link CreateProject }
     * 
     */
    public CreateProject createCreateProject() {
        return new CreateProject();
    }

    /**
     * Create an instance of {@link CreateProjectResponse }
     * 
     */
    public CreateProjectResponse createCreateProjectResponse() {
        return new CreateProjectResponse();
    }

    /**
     * Create an instance of {@link FindAllProjects }
     * 
     */
    public FindAllProjects createFindAllProjects() {
        return new FindAllProjects();
    }

    /**
     * Create an instance of {@link FindAllProjectsResponse }
     * 
     */
    public FindAllProjectsResponse createFindAllProjectsResponse() {
        return new FindAllProjectsResponse();
    }

    /**
     * Create an instance of {@link FindProjectById }
     * 
     */
    public FindProjectById createFindProjectById() {
        return new FindProjectById();
    }

    /**
     * Create an instance of {@link FindProjectByIdResponse }
     * 
     */
    public FindProjectByIdResponse createFindProjectByIdResponse() {
        return new FindProjectByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectById }
     * 
     */
    public RemoveProjectById createRemoveProjectById() {
        return new RemoveProjectById();
    }

    /**
     * Create an instance of {@link RemoveProjectByIdResponse }
     * 
     */
    public RemoveProjectByIdResponse createRemoveProjectByIdResponse() {
        return new RemoveProjectByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectById }
     * 
     */
    public UpdateProjectById createUpdateProjectById() {
        return new UpdateProjectById();
    }

    /**
     * Create an instance of {@link UpdateProjectByIdResponse }
     * 
     */
    public UpdateProjectByIdResponse createUpdateProjectByIdResponse() {
        return new UpdateProjectByIdResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ProjectDTO }
     * 
     */
    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountAllProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "countAllProjects")
    public JAXBElement<CountAllProjects> createCountAllProjects(CountAllProjects value) {
        return new JAXBElement<CountAllProjects>(_CountAllProjects_QNAME, CountAllProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountAllProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "countAllProjectsResponse")
    public JAXBElement<CountAllProjectsResponse> createCountAllProjectsResponse(CountAllProjectsResponse value) {
        return new JAXBElement<CountAllProjectsResponse>(_CountAllProjectsResponse_QNAME, CountAllProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "createProject")
    public JAXBElement<CreateProject> createCreateProject(CreateProject value) {
        return new JAXBElement<CreateProject>(_CreateProject_QNAME, CreateProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "createProjectResponse")
    public JAXBElement<CreateProjectResponse> createCreateProjectResponse(CreateProjectResponse value) {
        return new JAXBElement<CreateProjectResponse>(_CreateProjectResponse_QNAME, CreateProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "findAllProjects")
    public JAXBElement<FindAllProjects> createFindAllProjects(FindAllProjects value) {
        return new JAXBElement<FindAllProjects>(_FindAllProjects_QNAME, FindAllProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "findAllProjectsResponse")
    public JAXBElement<FindAllProjectsResponse> createFindAllProjectsResponse(FindAllProjectsResponse value) {
        return new JAXBElement<FindAllProjectsResponse>(_FindAllProjectsResponse_QNAME, FindAllProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "findProjectById")
    public JAXBElement<FindProjectById> createFindProjectById(FindProjectById value) {
        return new JAXBElement<FindProjectById>(_FindProjectById_QNAME, FindProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "findProjectByIdResponse")
    public JAXBElement<FindProjectByIdResponse> createFindProjectByIdResponse(FindProjectByIdResponse value) {
        return new JAXBElement<FindProjectByIdResponse>(_FindProjectByIdResponse_QNAME, FindProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "removeProjectById")
    public JAXBElement<RemoveProjectById> createRemoveProjectById(RemoveProjectById value) {
        return new JAXBElement<RemoveProjectById>(_RemoveProjectById_QNAME, RemoveProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "removeProjectByIdResponse")
    public JAXBElement<RemoveProjectByIdResponse> createRemoveProjectByIdResponse(RemoveProjectByIdResponse value) {
        return new JAXBElement<RemoveProjectByIdResponse>(_RemoveProjectByIdResponse_QNAME, RemoveProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "updateProjectById")
    public JAXBElement<UpdateProjectById> createUpdateProjectById(UpdateProjectById value) {
        return new JAXBElement<UpdateProjectById>(_UpdateProjectById_QNAME, UpdateProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "updateProjectByIdResponse")
    public JAXBElement<UpdateProjectByIdResponse> createUpdateProjectByIdResponse(UpdateProjectByIdResponse value) {
        return new JAXBElement<UpdateProjectByIdResponse>(_UpdateProjectByIdResponse_QNAME, UpdateProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.ermolaev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

}
