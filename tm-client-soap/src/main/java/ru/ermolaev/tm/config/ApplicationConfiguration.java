package ru.ermolaev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ermolaev.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.ermolaev.tm.endpoint.soap.ProjectSoapEndpointService;
import ru.ermolaev.tm.endpoint.soap.TaskSoapEndpoint;
import ru.ermolaev.tm.endpoint.soap.TaskSoapEndpointService;

@Configuration
@ComponentScan(basePackages = "ru.ermolaev.tm")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public TaskSoapEndpointService taskSoapEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    @NotNull
    public TaskSoapEndpoint taskSoapEndpoint(
            @NotNull @Autowired final TaskSoapEndpointService taskSoapEndpointService
    ) {
        return taskSoapEndpointService.getTaskSoapEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectSoapEndpointService projectSoapEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    @NotNull
    public ProjectSoapEndpoint projectSoapEndpoint(
            @NotNull @Autowired final ProjectSoapEndpointService projectSoapEndpointService
    ) {
        return projectSoapEndpointService.getProjectSoapEndpointPort();
    }

}
