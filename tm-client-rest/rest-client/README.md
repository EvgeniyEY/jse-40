# swagger-java-client

Swagger Maven Plugin Sample
- API version: v1
  - Build date: 2021-02-10T17:46:04.385+03:00

This is a sample for swagger-maven-plugin

  For more information, please visit [http://www.ermolaev.ru](http://www.ermolaev.ru)

*Automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen)*


## Requirements

Building the API client library requires:
1. Java 1.7+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>io.swagger</groupId>
  <artifactId>swagger-java-client</artifactId>
  <version>1.0.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/swagger-java-client-1.0.0.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.DefaultApi;

import java.io.File;
import java.util.*;

public class DefaultApiExample {

    public static void main(String[] args) {
        
        DefaultApi apiInstance = new DefaultApi();
        try {
            Long result = apiInstance.countAllProjects();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#countAllProjects");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**countAllProjects**](docs/DefaultApi.md#countAllProjects) | **GET** /rest/project/countAll | 
*DefaultApi* | [**countAllTasks**](docs/DefaultApi.md#countAllTasks) | **GET** /rest/task/countAll | 
*DefaultApi* | [**createProject**](docs/DefaultApi.md#createProject) | **POST** /rest/project/create | 
*DefaultApi* | [**createTask**](docs/DefaultApi.md#createTask) | **POST** /rest/task/create | 
*DefaultApi* | [**findAll**](docs/DefaultApi.md#findAll) | **GET** /rest/project/findAll | 
*DefaultApi* | [**findAll_0**](docs/DefaultApi.md#findAll_0) | **GET** /rest/task/findAll | 
*DefaultApi* | [**findById**](docs/DefaultApi.md#findById) | **GET** /rest/project/findById/{id} | 
*DefaultApi* | [**findById_0**](docs/DefaultApi.md#findById_0) | **GET** /rest/task/findById/{id} | 
*DefaultApi* | [**removeById**](docs/DefaultApi.md#removeById) | **DELETE** /rest/project/removeById/{id} | 
*DefaultApi* | [**removeOneById**](docs/DefaultApi.md#removeOneById) | **DELETE** /rest/task/removeById/{id} | 
*DefaultApi* | [**updateById**](docs/DefaultApi.md#updateById) | **PUT** /rest/project/updateById | 
*DefaultApi* | [**updateById_0**](docs/DefaultApi.md#updateById_0) | **PUT** /rest/task/updateById | 


## Documentation for Models

 - [ProjectDTO](docs/ProjectDTO.md)
 - [TaskDTO](docs/TaskDTO.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### basicAuth

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

ermolaev@yandex.ru

