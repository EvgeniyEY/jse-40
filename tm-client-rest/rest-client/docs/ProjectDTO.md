
# ProjectDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]
**startDate** | **String** |  |  [optional]
**completeDate** | **String** |  |  [optional]
**creationDate** | **String** |  |  [optional]



