<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">TASK LIST</p>

<table border="1" width="100%" margin-top="0px">
    <tr>
        <td class="table-head" width="25%">ID</td>
        <td class="table-head" width="20%">Task Name</td>
        <td class="table-head" width="31%">Description</td>
        <td class="table-head" width="8%">View</td>
        <td class="table-head" width="8%">Update</td>
        <td class="table-head" width="8%">Remove</td>
    </tr>
    <c:forEach items="${tasks}" var="task">
        <tr>
            <td class="td_list"><c:out value="${task.id}"/></td>
            <td class="td_list"><c:out value="${task.name}"/></td>
            <td class="td_list"><c:out value="${task.description}"/></td>
            <td style="font-size: 20px" align="center"><a href="/tasks/view/${task.id}">View</a></td>
            <td style="font-size: 20px" align="center"><a href="/tasks/update/${task.id}">Update</a></td>
            <td style="font-size: 20px" align="center"><a href="/tasks/remove/${task.id}">Remove</a></td>
        </tr>
    </c:forEach>
</table>
<div style="margin-top: 10px;">
    <a href="/tasks/create" class="button">Create new task</a>
</div>
<div style="margin-top: 10px;">
    <a href="/" class="button">Main page</a>
</div>

<jsp:include page="../resources/_postPage.jsp"/>