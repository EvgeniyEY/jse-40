package ru.ermolaev.tm.constant;

public class TimeConstant {

    public static final String TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";

    public static final String TIMEZONE = "Europe/Moscow";

}
