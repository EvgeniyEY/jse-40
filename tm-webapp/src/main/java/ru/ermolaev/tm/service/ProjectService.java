package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.ermolaev.tm.exception.incorrect.IncorrectStartDateException;
import ru.ermolaev.tm.repository.IProjectRepository;

import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IUserService userService;

    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectService(
            @NotNull final IUserService userService,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.userService = userService;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Project getOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.getOne(id);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project createProject(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.getOneById(userId));
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @Nullable final Project project = findById(id);
        if (project == null) return null;
        if (!name.isEmpty()) project.setName(name);
        if (!description.isEmpty()) project.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return null;
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        if (!name.isEmpty()) project.setName(name);
        if (!description.isEmpty()) project.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return null;
        projectRepository.save(project);
        return project;
    }

    @Override
    @Transactional
    public void updateStartDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        project.setStartDate(date);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void updateCompleteDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        if (project.getStartDate() == null) throw new IncorrectCompleteDateException(date);
        if (project.getStartDate().after(date)) throw new IncorrectCompleteDateException(date);
        project.setCompleteDate(date);
        projectRepository.save(project);
    }

    @NotNull
    @Override
    public Long countAllProjects() {
        @NotNull final Long countOfProjects = projectRepository.count();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Long countUserProjects(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Long countOfProjects = projectRepository.countByUserId(userId);
        return countOfProjects;
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(id).orElse(null);
        return project;
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, id);
        return project;
    }

    @Nullable
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findByUserIdAndName(userId, name);
        return project;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(projectRepository.findAll());
        return projectsDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(projectRepository.findAllByUserId(userId));
        return projectsDTO;
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

}
