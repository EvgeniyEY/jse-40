package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void export(@Nullable Domain domain);

}
