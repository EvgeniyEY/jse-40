package ru.ermolaev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ermolaev.tm.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}
