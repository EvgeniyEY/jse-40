package ru.ermolaev.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    private final IProjectService projectService;

    private final IUserService userService;

    private final ITaskService taskService;

    @Autowired
    public ProjectController(
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.userService = userService;
        this.taskService = taskService;
    }

    @GetMapping("/show")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectList");
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") @NotNull final String id) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectView");
        @Nullable final Project project = projectService.findById(id);
        modelAndView.addObject("project", project);
        modelAndView.addObject("tasks", taskService.findAllByProjectId(id));
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectCreate");
        modelAndView.addObject("users", userService.findAll());
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("project") @NotNull final Project project,
            @RequestParam(value = "user_id") @NotNull final String user_id
    ) throws Exception {
        projectService.createProject(user_id, project.getName(), project.getDescription());
        return new ModelAndView("redirect:/projects/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) throws Exception {
        projectService.removeOneById(id);
        return new ModelAndView("redirect:/projects/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectUpdate");
        @Nullable final Project project = projectService.findById(id);
        modelAndView.addObject("project", project);
        modelAndView.addObject("users", userService.findAll());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("project") @NotNull final Project project
    ) throws Exception {
        projectService.updateById(id, project.getName(), project.getDescription());
        return new ModelAndView("redirect:/projects/show");
    }

}
