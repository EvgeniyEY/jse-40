package ru.ermolaev.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final ITaskService taskService;

    private final IUserService userService;

    private final IProjectService projectService;

    @Autowired
    public TaskController(
            @NotNull final ITaskService taskService,
            @NotNull final IUserService userService,
            @NotNull final IProjectService projectService
    ) {
        this.taskService = taskService;
        this.userService = userService;
        this.projectService = projectService;
    }

    @GetMapping("/show")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskList");
        modelAndView.addObject("tasks", taskService.findAll());
        return modelAndView;
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") @NotNull final String id) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskView");
        @Nullable final Task task = taskService.findById(id);
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskCreate");
        modelAndView.addObject("users", userService.findAll());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final Task task,
            @RequestParam(value = "user_id") @NotNull final String user_id,
            @RequestParam(value = "project_id") @NotNull final String project_id
    ) throws Exception {
        taskService.createTask(user_id, task.getName(), project_id, task.getDescription());
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) throws Exception {
        taskService.removeOneById(id);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskUpdate");
        @Nullable final Task task = taskService.findById(id);
        modelAndView.addObject("task", task);
        modelAndView.addObject("users", userService.findAll());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("task") @NotNull final Task task
    ) throws Exception {
        taskService.updateById(id, task.getName(), task.getDescription());
        return new ModelAndView("redirect:/tasks/show");
    }

}