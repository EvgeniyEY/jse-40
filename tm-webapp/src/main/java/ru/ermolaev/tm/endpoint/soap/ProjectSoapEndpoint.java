package ru.ermolaev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.createProject(
                projectDTO.getUserId(),
                projectDTO.getName(),
                projectDTO.getDescription()));
    }

    @Nullable
    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.updateById(
                projectDTO.getId(),
                projectDTO.getName(),
                projectDTO.getDescription()));
    }

    @NotNull
    @WebMethod
    public Long countAllProjects() {
        return projectService.countAllProjects();
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.findById(id));
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects() {
        return projectService.findAll();
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        projectService.removeOneById(id);
    }

}
